<?php

include "autoload.php";
spl_autoload_register('basicClassLoader');

$app = app\App::getInstance();
try {
    $app->run();
} catch (\app\exceptions\MyClassNotFoundException $e) {
    http_response_code (404);
    die ('wrong request');
} catch (Exception $e) {
    http_response_code (500);
    die ("internal server error");
}
