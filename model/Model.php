<?php


namespace models;


use app\exceptions\MyDataException;

class Model
{
    protected $data;

    public function rules()
    {
        return [];
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        } else {
            $classname = get_class($this);
            throw new MyDataException("Trying to get unknown field $classname::$name");
        }
    }

    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->data)) {
            $this->data[$name] = $value;
        } else {
            $classname = get_class($this);
            throw new MyDataException("Trying to get unknown field $classname::$name");
        }
    }

    public function validate($name, $value)
    {

    }
}