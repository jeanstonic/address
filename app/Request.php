<?php


namespace app;


use app\exceptions\MyRequestException;

class Request
{
    protected $uri;

    public function __construct()
    {
        $this->parseUri();
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    protected function parseUri()
    {
        //we assume that all requests looks like domain.com/model or domain.com/model/<id>
        $this->uri = explode('/', $_SERVER['REQUEST_URI']);
        array_shift($this->uri);
    }

    /**
     * @param int $index
     * @return mixed
     * @throws MyRequestException
     */
    public function getUri($index = -1)
    {
        if ($index == -1) {
            return $this->uri;
        } elseif (array_key_exists($index, $this->uri)) {
            return $this->uri[$index];
        } else {
            throw new MyRequestException('incorrect index');
        }
    }
}