<?php


namespace App;


use app\exceptions\MyClassNotFoundException;
use app\exceptions\MyException;
use app\exceptions\MyRequestException;
use app\exceptions\MyRequestMethodException;
use controllers\SiteController;

class App
{
    protected static $instance = null;

    public $controller;
    public $request;
    public $requestedId = null;

    protected function __construct()
    {
        $this->request = new Request();
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        try {
            $this->selectController();
            $actionName = $this->selectAction();
            if ($this->requestedId) {
                $this->controller->$actionName($this->requestedId);
            } else {
                $this->controller->$actionName();
            }
        } catch (MyRequestMethodException $e) {
            http_response_code (404);
            die ('wrong request method');
        } catch (MyException $e) {
            http_response_code (404);
            die ('wrong request');
        }
    }

    function selectController()
    {
        if (count($this->request->getUri()) == 1 && $this->request->getUri(0) == '') {
            $this->controller = new SiteController();
        } else {
            $controllerName = 'controllers\\' . ucfirst($this->request->getUri(0)) . 'Controller';
            if (!class_exists ($controllerName)) {
                throw new MyClassNotFoundException('class not exists');
            }
            $this->controller = new $controllerName();
        }
    }

    /**
     * @return string
     * @throws MyRequestException
     */
    function selectAction()
    {
        if (count($this->request->getUri()) == 1) {
            switch ($this->request->getMethod()) {
                case "GET":
                    return 'actionIndex';
                case "POST":
                    return 'actionCreate';
                default:
                    throw new MyRequestException('incorrect request method');
            }
        } elseif (count($this->request->getUri()) == 2) {
            if (!ctype_digit ($this->request->getUri(1))) {
                throw new MyRequestException('non numeric id');
            }
            $this->requestedId = (int) $this->request->getUri(1);
            switch ($this->request->getMethod()) {
                case "GET":
                    return 'actionView';
                case "PUT":
                    return 'actionUpdate';
                case "DELETE":
                    return 'actionDelete';
                default:
                    throw new MyRequestMethodException('incorrect request method');
            }
        }
    }
}