<?php

/**
 * @param $className
 * @throws \app\exceptions\MyClassNotFoundException
 */
function basicClassLoader($className)
{
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    if (!file_exists("$className.php")) {
        throw new app\exceptions\MyClassNotFoundException("class $className not found");
    }
    include "$className.php";
}