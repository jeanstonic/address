<?php

namespace controllers;

class AddressController
{
    public function actionIndex()
    {
        echo "list";
    }

    public function actionCreate()
    {
        echo "create";
    }

    public function actionUpdate($id)
    {
        echo "update";
    }

    public function actionDelete($id)
    {
        echo "delete";
    }

    public function actionView($id)
    {
        echo "view";
    }
}